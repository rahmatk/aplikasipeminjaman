package net.codejava.spring.controller;

import net.codejava.spring.dao.AnggotaDAO;
import net.codejava.spring.model.Anggota;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * Created by yoggi on 5/23/2017.
 */
@Controller
public class AnggotaController {
    @Autowired
    private AnggotaDAO anggotaDAO;

    @RequestMapping(value="/listAnggota")
    public ModelAndView listAnggota(ModelAndView model) throws IOException {
        List<Anggota> listAnggota = anggotaDAO.list();
        model.addObject("listAnggota", listAnggota);
        model.setViewName("anggota");

        return model;
    }

    @RequestMapping(value = "/newAnggota", method = RequestMethod.GET)
    public ModelAndView newAnggota(ModelAndView model) {
        Anggota newAnggota = new Anggota();
        model.addObject("anggota", newAnggota);
        model.setViewName("AnggotaForm");
        return model;
    }

    @RequestMapping(value = "/saveAnggota", method = RequestMethod.POST)
    public ModelAndView saveAnggota(@ModelAttribute Anggota anggota) {
        anggotaDAO.saveOrUpdate(anggota);
        return new ModelAndView("redirect:/listAnggota");
    }

    @RequestMapping(value = "/deleteAnggota", method = RequestMethod.GET)
    public ModelAndView deleteAnggota(HttpServletRequest request) {
        int anggotaId = Integer.parseInt(request.getParameter("id"));
        anggotaDAO.delete(anggotaId);
        return new ModelAndView("redirect:/listAnggota");
    }

    @RequestMapping(value = "/editAnggota", method = RequestMethod.GET)
    public ModelAndView editAnggota(HttpServletRequest request) {
        int anggotaId = Integer.parseInt(request.getParameter("id"));
        Anggota anggota = anggotaDAO.get(anggotaId);
        ModelAndView model = new ModelAndView("AnggotaForm");
        model.addObject("anggota", anggota);

        return model;
    }
/*
    @RequestMapping(value="/searchAnggota", method = RequestMethod.GET)
    public ModelAndView SearchAnggota(HttpServletRequest request){
        String namaAnggota = String.valueOf(request.getParameter("nama"));
        Anggota anggota = anggotaDAO.find(namaAnggota);


        return model;
    }
*/

}
