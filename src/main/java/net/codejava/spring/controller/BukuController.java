package net.codejava.spring.controller;

/**
 * Created by Rahmat on 5/22/2017.
 */
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.codejava.spring.dao.BukuDAO;
import net.codejava.spring.model.Buku;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BukuController {
    @Autowired
    private BukuDAO bukuDAO;

    @RequestMapping(value="/listBuku")
    public ModelAndView listBuku(ModelAndView model) throws IOException{
        List<Buku> listBuku = bukuDAO.list();
        model.addObject("listBuku", listBuku);
        model.setViewName("buku");

        return model;
    }

    @RequestMapping(value = "/newBuku", method = RequestMethod.GET)
    public ModelAndView newBuku(ModelAndView model) {
        Buku newBuku = new Buku();
        model.addObject("buku", newBuku);
        model.setViewName("BukuForm");
        return model;
    }

    @RequestMapping(value = "/saveBuku", method = RequestMethod.POST)
    public ModelAndView saveBuku(@ModelAttribute Buku buku) {
        bukuDAO.saveOrUpdate(buku);
        return new ModelAndView("redirect:/listBuku");
    }

    @RequestMapping(value = "/deleteBuku", method = RequestMethod.GET)
    public ModelAndView deleteBuku(HttpServletRequest request) {
        int bukuId = Integer.parseInt(request.getParameter("id"));
        bukuDAO.delete(bukuId);
        return new ModelAndView("redirect:/listBuku");
    }

    @RequestMapping(value = "/editBuku", method = RequestMethod.GET)
    public ModelAndView editBuku(HttpServletRequest request) {
        int bukuId = Integer.parseInt(request.getParameter("id"));
        Buku buku = bukuDAO.get(bukuId);
        ModelAndView model = new ModelAndView("BukuForm");
        model.addObject("buku", buku);

        return model;
    }
}
