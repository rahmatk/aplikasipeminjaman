package net.codejava.spring.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.sun.org.apache.regexp.internal.RE;
import net.codejava.spring.dao.AnggotaDAO;
import net.codejava.spring.dao.BukuDAO;
import net.codejava.spring.dao.LoginDAO;
import net.codejava.spring.dao.PetugasDAO;
import net.codejava.spring.model.Anggota;
import net.codejava.spring.model.Buku;
import net.codejava.spring.model.Login;
import net.codejava.spring.model.Petugas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/* Controller menghubungkan model dan view */

@Controller
public class HomeController {
	@Autowired
	private LoginDAO loginDAO;
	@Autowired
	private BukuDAO bukuDAO;
	@Autowired
	private AnggotaDAO anggotaDAO;
	@Autowired
	private PetugasDAO petugasDAO;

	@RequestMapping(value="/")
	public ModelAndView home(ModelAndView model) throws IOException{
		model.setViewName("login");
		return model;
	}

	@RequestMapping(value = "/bukulist")
	public ModelAndView bukulist(ModelAndView model) throws IOException{
		List<Buku> listBuku = bukuDAO.list();
		model.addObject("listBuku", listBuku);
		model.setViewName("bukulist");

		return model;
	}

	@RequestMapping(value = "/anggotalist")
	public ModelAndView anggotalist(ModelAndView model) throws IOException{
		List<Anggota> listAnggota = anggotaDAO.list();
		model.addObject("listAnggota", listAnggota);
		model.setViewName("anggotalist");

		return model;
	}

	@RequestMapping(value = "/petugaslist")
	public ModelAndView petugaslist(ModelAndView model) throws IOException{
		List<Petugas> listPetugas = petugasDAO.list();
		model.addObject("listPetugas", listPetugas);
		model.setViewName("petugaslist");

		return model;
	}

	@RequestMapping(value="/dashboard")
	public ModelAndView dashboard(ModelAndView model) throws IOException{
		model.setViewName("dashboard");
		return model;
	}

	@RequestMapping(value="/verifyLog")
	public ModelAndView verifyLog(ModelAndView model) throws IOException{
		model.setViewName("dashboard");
		return model;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView login(HttpServletRequest request) {
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		Login login = loginDAO.get(username,password);
		if(login != null){
			ModelAndView model = new ModelAndView("dashboard");
			model.addObject("login",login);
			return model;
		}
		else {
			ModelAndView model = new ModelAndView("login");
			model.addObject("msg","Invalid username or password!");
			return model;
		}
	}
}
