package net.codejava.spring.controller;

import net.codejava.spring.dao.PeminjamanDAO;
import net.codejava.spring.dao.PetugasDAO;
import net.codejava.spring.dao.AnggotaDAO;
import net.codejava.spring.dao.BukuDAO;
import net.codejava.spring.model.Anggota;
import net.codejava.spring.model.Buku;
import net.codejava.spring.model.Peminjaman;
import net.codejava.spring.model.Petugas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * Created by yoggi on 5/23/2017.
 */
@Controller
public class PeminjamanController {
    @Autowired
    private PeminjamanDAO peminjamanDAO;
    @Autowired
    private BukuDAO bukuDAO;
    @Autowired
    private AnggotaDAO anggotaDAO;
    @Autowired
    private PetugasDAO petugasDAO;

    @RequestMapping(value = "/listPeminjaman")
    public ModelAndView listPeminjaman(ModelAndView model) throws IOException {
        List<Peminjaman> listPeminjaman = peminjamanDAO.list();
        model.addObject("listPeminjaman", listPeminjaman);
        model.setViewName("peminjaman");
        return model;
    }

    @RequestMapping(value = "/newPeminjaman", method = RequestMethod.GET)
    public ModelAndView newPeminjaman(ModelAndView model) {
        Peminjaman newPeminjaman = new Peminjaman();
        model.addObject("peminjaman", newPeminjaman);
        model.setViewName("PeminjamanForm");
        return model;
    }

    @RequestMapping(value = "/savePeminjaman", method = RequestMethod.POST)
    public ModelAndView savePeminjaman(@ModelAttribute Peminjaman peminjaman) {
        peminjamanDAO.saveOrUpdate(peminjaman);
        return new ModelAndView("redirect:/listPeminjaman");
    }

    @RequestMapping(value = "/deletePeminjaman", method = RequestMethod.GET)
    public ModelAndView deletePeminjaman(HttpServletRequest request) {
        int peminjamanId = Integer.parseInt(request.getParameter("id"));
        peminjamanDAO.delete(peminjamanId);
        return new ModelAndView("redirect:/listPeminjaman");
    }

    @RequestMapping(value = "/editPeminjaman", method = RequestMethod.GET)
    public ModelAndView editPeminjaman(HttpServletRequest request) {
        int peminjamanId = Integer.parseInt(request.getParameter("id"));
        Peminjaman peminjaman = peminjamanDAO.get(peminjamanId);
        ModelAndView model = new ModelAndView("PeminjamanForm");
        model.addObject("peminjaman", peminjaman);
        return model;
    }
}