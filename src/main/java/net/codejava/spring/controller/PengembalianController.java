package net.codejava.spring.controller;

import net.codejava.spring.dao.*;
import net.codejava.spring.model.Peminjaman;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

/**
 * Created by yoggi on 5/23/2017.
 */
@Controller
public class PengembalianController {
    @Autowired
    private PengembalianDAO pengembalianDAO;
    @Autowired
    private PeminjamanDAO peminjamanDAO;
    @Autowired
    private BukuDAO bukuDAO;
    @Autowired
    private AnggotaDAO anggotaDAO;
    @Autowired
    private PetugasDAO petugasDAO;

    @RequestMapping(value = "/listPengembalian")
    public ModelAndView listPeminjaman(ModelAndView model) throws IOException {
        List<Peminjaman> listPeminjaman = pengembalianDAO.list();
        model.addObject("listPeminjaman", listPeminjaman);
        model.setViewName("pengembalian");
        return model;
    }

    @RequestMapping(value = "/savePengembalian", method = RequestMethod.POST)
    public ModelAndView savePeminjaman(@ModelAttribute Peminjaman peminjaman) {
        pengembalianDAO.saveOrUpdate(peminjaman);
        return new ModelAndView("redirect:/listPengembalian");
    }

    @RequestMapping(value = "/editPengembalian", method = RequestMethod.GET)
    public ModelAndView editPeminjaman(HttpServletRequest request) {
        int peminjamanId = Integer.parseInt(request.getParameter("id"));
        Peminjaman peminjaman = pengembalianDAO.get(peminjamanId);
        ModelAndView model = new ModelAndView("PengembalianForm");
        model.addObject("peminjaman", peminjaman);
        return model;
    }
}