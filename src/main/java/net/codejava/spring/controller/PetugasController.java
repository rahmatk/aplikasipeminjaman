package net.codejava.spring.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.codejava.spring.dao.PetugasDAO;
import net.codejava.spring.model.Petugas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PetugasController {
    @Autowired
    private PetugasDAO petugasDAO;

    @RequestMapping(value="/listPetugas")
    public ModelAndView listPetugas(ModelAndView model) throws IOException{
        List<Petugas> listPetugas = petugasDAO.list();
        model.addObject("listPetugas", listPetugas);
        model.setViewName("petugas");
        return model;
    }

    @RequestMapping(value="/newPetugas", method = RequestMethod.GET)
    public ModelAndView newPetugas(ModelAndView model){
        Petugas newPetugas = new Petugas();
        model.addObject("petugas", newPetugas);
        model.setViewName("PetugasForm");
        return model;
    }

    @RequestMapping(value = "/savePetugas", method = RequestMethod.POST)
    public ModelAndView savePetugas(@ModelAttribute Petugas petugas){
        petugasDAO.saveOrUpdate(petugas);
        return new ModelAndView("redirect:/listPetugas");
    }

    @RequestMapping(value = "/deletePetugas", method = RequestMethod.GET)
    public ModelAndView deletePetugas(HttpServletRequest request) {
        int petugasId = Integer.parseInt(request.getParameter("id"));
        petugasDAO.delete(petugasId);
        return new ModelAndView("redirect:/listPetugas");
    }

    @RequestMapping(value = "/editPetugas", method = RequestMethod.GET)
    public ModelAndView editPetugas(HttpServletRequest request) {
        int petugasId = Integer.parseInt(request.getParameter("id"));
        Petugas petugas = petugasDAO.get(petugasId);
        ModelAndView model = new ModelAndView("PetugasForm");
        model.addObject("petugas", petugas);

        return model;
    }

}
