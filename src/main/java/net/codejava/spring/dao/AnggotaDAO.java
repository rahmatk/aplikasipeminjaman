package net.codejava.spring.dao;

import net.codejava.spring.model.Anggota;

import java.util.List;

/**
 * Created by yoggi on 5/23/2017.
 */
public interface AnggotaDAO {
    public void saveOrUpdate(Anggota anggota);

    public void delete(int anggotaId);

    public Anggota get(int anggotaId);

    public List<Anggota> list();

}
