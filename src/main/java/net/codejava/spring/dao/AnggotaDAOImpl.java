package net.codejava.spring.dao;

import net.codejava.spring.model.Anggota;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by yoggi on 5/23/2017.
 */
public class AnggotaDAOImpl implements AnggotaDAO {
    private JdbcTemplate jdbcTemplate;
    public AnggotaDAOImpl(DataSource dataSource) { jdbcTemplate = new JdbcTemplate(dataSource); }

    @Override
    public void saveOrUpdate(Anggota anggota) {
        if (anggota.getIdAnggota() > 0) {
            // update
            String sql = "UPDATE anggota SET nama=?, alamat=?, kontak=?, "
                    + "email=? WHERE id_anggota=?";
            jdbcTemplate.update(sql, anggota.getNama(), anggota.getAlamat(),
                    anggota.getKontak(), anggota.getEmail(), anggota.getIdAnggota());
        } else {
            // insert
            String sql = "INSERT INTO anggota (nama, alamat, kontak, email)"
                    + " VALUES (?, ?, ?, ?)";
            jdbcTemplate.update(sql, anggota.getNama(), anggota.getAlamat(),
                    anggota.getKontak(), anggota.getEmail());
        }

    }

    @Override
    public void delete(int anggotaId) {
        String sql = "DELETE FROM anggota WHERE id_anggota=?";
        jdbcTemplate.update(sql, anggotaId);
    }

    @Override
    public List<Anggota> list() {
        String sql = "SELECT * FROM anggota";
        List<Anggota> listAnggota = jdbcTemplate.query(sql, new RowMapper<Anggota>() {

            @Override
            public Anggota mapRow(ResultSet rs, int rowNum) throws SQLException {
                Anggota anggotas = new Anggota();

                anggotas.setIdAnggota(rs.getInt("id_anggota"));
                anggotas.setNama(rs.getString("nama"));
                anggotas.setAlamat(rs.getString("alamat"));
                anggotas.setKontak(rs.getString("kontak"));
                anggotas.setEmail(rs.getString("email"));

                return anggotas;
            }

        });

        return listAnggota;
    }



    @Override
    public Anggota get(int anggotaId) {
        String sql = "SELECT * FROM anggota WHERE id_anggota=" + anggotaId;
        return jdbcTemplate.query(sql, new ResultSetExtractor<Anggota>() {

            @Override
            public Anggota extractData(ResultSet rs) throws SQLException,
                    DataAccessException {
                if (rs.next()) {
                    Anggota anggota = new Anggota();
                    anggota.setIdAnggota(rs.getInt("id_anggota"));
                    anggota.setNama(rs.getString("nama"));
                    anggota.setAlamat(rs.getString("alamat"));
                    anggota.setKontak(rs.getString("kontak"));
                    anggota.setEmail(rs.getString("email"));
                    return anggota;
                }

                return null;
            }

        });
    }
}



