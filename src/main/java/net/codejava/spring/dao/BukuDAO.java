package net.codejava.spring.dao;

import net.codejava.spring.model.Buku;

import java.util.List;

/* DAO untuk contact model */

public interface BukuDAO {
	
	public void saveOrUpdate(Buku buku);
	
	public void delete(int bukuId);
	
	public Buku get(int bukuId);
	
	public List<Buku> list();
}
