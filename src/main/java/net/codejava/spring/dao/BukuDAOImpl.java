package net.codejava.spring.dao;

import net.codejava.spring.model.Buku;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * query dari contact DAO 
 */
public class BukuDAOImpl implements BukuDAO {

	private JdbcTemplate jdbcTemplate;

	public BukuDAOImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public void saveOrUpdate(Buku buku) {
		if (buku.getId_buku() > 0) {
			// update
			String sql = "UPDATE buku SET judul=?, penulis=?, penerbit=?, "
						+ "tahun=? WHERE id_buku=?";
			jdbcTemplate.update(sql, buku.getJudul(), buku.getPenulis(),
					buku.getPenerbit(), buku.getTahun(), buku.getId_buku());
		} else {
			// insert
			String sql = "INSERT INTO buku (judul, penulis, penerbit, tahun)"
						+ " VALUES (?, ?, ?, ?)";
			jdbcTemplate.update(sql, buku.getJudul(), buku.getPenulis(),
					buku.getPenerbit(), buku.getTahun());
		}
		
	}

	@Override
	public void delete(int bukuId) {
		String sql = "DELETE FROM buku WHERE id_buku=?";
		jdbcTemplate.update(sql, bukuId);
	}

	@Override
	public List<Buku> list() {
		String sql = "SELECT * FROM buku";
		List<Buku> listBuku = jdbcTemplate.query(sql, new RowMapper<Buku>() {

			@Override
			public Buku mapRow(ResultSet rs, int rowNum) throws SQLException {
				Buku aBuku = new Buku();
	
				aBuku.setId_buku(rs.getInt("id_buku"));
				aBuku.setJudul(rs.getString("judul"));
				aBuku.setPenulis(rs.getString("penulis"));
				aBuku.setPenerbit(rs.getString("penerbit"));
				aBuku.setTahun(rs.getString("tahun"));
				
				return aBuku;
			}
			
		});
		
		return listBuku;
	}

	@Override
	public Buku get(int bukuId) {
		String sql = "SELECT * FROM buku WHERE id_buku=" + bukuId;
		return jdbcTemplate.query(sql, new ResultSetExtractor<Buku>() {

			@Override
			public Buku extractData(ResultSet rs) throws SQLException,
					DataAccessException {
				if (rs.next()) {
					Buku buku = new Buku();
					buku.setId_buku(rs.getInt("id_buku"));
					buku.setJudul(rs.getString("judul"));
					buku.setPenulis(rs.getString("penulis"));
					buku.setPenerbit(rs.getString("penerbit"));
					buku.setTahun(rs.getString("tahun"));
					return buku;
				}
				
				return null;
			}
			
		});
	}

}
