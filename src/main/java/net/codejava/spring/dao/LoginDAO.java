package net.codejava.spring.dao;

import net.codejava.spring.model.Login;

/**
 * Created by Rahmat on 5/29/2017.
 */
public interface LoginDAO {
    public Login get(String username, String password);

}
