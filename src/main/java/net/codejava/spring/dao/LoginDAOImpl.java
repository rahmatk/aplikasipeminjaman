package net.codejava.spring.dao;

import net.codejava.spring.model.Login;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class LoginDAOImpl implements LoginDAO {
    private JdbcTemplate jdbcTemplate;
    public LoginDAOImpl(DataSource dataSource) { jdbcTemplate = new JdbcTemplate(dataSource); }

    @Override
    public Login get(String username, String password) {
        String sql = "SELECT username, password FROM akseslogin WHERE username = '" + username + "' AND password = '" + password + "'";
        return jdbcTemplate.query(sql, new ResultSetExtractor<Login>() {

            @Override
            public Login extractData(ResultSet rs) throws SQLException,
                    DataAccessException {
                if (rs.next()) {
                    Login login = new Login();
                    login.setUsername(rs.getString("username"));
                    login.setPassword(rs.getString("password"));
                    return login;
                }
                return null;
            }

        });
    }

}
