package net.codejava.spring.dao;

import net.codejava.spring.model.Anggota;
import net.codejava.spring.model.Buku;
import net.codejava.spring.model.Peminjaman;
import net.codejava.spring.model.Petugas;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by yoggi on 5/23/2017.
 */
public class PeminjamanDAOImpl implements PeminjamanDAO {

    private JdbcTemplate jdbcTemplate;

    public PeminjamanDAOImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void saveOrUpdate(Peminjaman peminjaman) {
        if (peminjaman.getIdPeminjaman() > 0) {
            // update
            String sql = "UPDATE peminjaman SET kode_peminjaman=?, tanggal_peminjaman=?, id_buku=?, id_anggota=?, "
                    + "id_petugas=? WHERE id_peminjaman=?";
            jdbcTemplate.update(sql, peminjaman.getKodePeminjaman(), peminjaman.getTanggalPinjam(), peminjaman.getBuku().getId_buku(), peminjaman.getAnggota().getIdAnggota(), peminjaman.getPetugas().getId_petugas(), peminjaman.getIdPeminjaman());
        } else {
            // insert
            String sql = "INSERT into peminjaman (kode_peminjaman, tanggal_peminjaman, tanggal_kembali, id_buku, id_anggota, id_petugas)"
                    + "VALUES (?,?,?,?,?,?)";
            jdbcTemplate.update(sql, peminjaman.getKodePeminjaman(), peminjaman.getTanggalPinjam(), "null", peminjaman.getBuku().getId_buku(), peminjaman.getAnggota().getIdAnggota(), peminjaman.getPetugas().getId_petugas());

        }
    }

    @Override
    public void delete(int peminjamanId) {
        String sql = "DELETE FROM peminjaman WHERE id_peminjaman=?";
        jdbcTemplate.update(sql, peminjamanId);
    }

    @Override
    public List<Peminjaman> list() {
        String sql = "SELECT * FROM peminjaman, buku, anggota, petugas "+
                "WHERE peminjaman.id_buku = buku.id_buku " +
                "AND peminjaman.id_anggota = anggota.id_anggota " +
                "AND peminjaman.id_petugas = petugas.id_petugas " +
                "ORDER BY peminjaman.kode_peminjaman DESC";

        List<Peminjaman> listPeminjaman = jdbcTemplate.query(sql, new RowMapper<Peminjaman>() {

            @Override
            public Peminjaman mapRow(ResultSet rs, int rowNum) throws SQLException {
                Peminjaman peminjamans = new Peminjaman();
                peminjamans.setIdPeminjaman(rs.getInt("id_peminjaman"));
                peminjamans.setKodePeminjaman(rs.getString("kode_peminjaman"));
                peminjamans.setTanggalPinjam(rs.getString("tanggal_peminjaman"));

                Buku bukus = new Buku();
                bukus.setId_buku(rs.getInt("id_buku"));
                bukus.setJudul(rs.getString("judul"));
                peminjamans.setBuku(bukus);

                Anggota anggotas = new Anggota();
                anggotas.setIdAnggota(rs.getInt("id_anggota"));
                anggotas.setNama(rs.getString("nama"));
                peminjamans.setAnggota(anggotas);

                Petugas petugass = new Petugas();
                petugass.setId_petugas(rs.getInt("id_petugas"));
                petugass.setNama(rs.getString("nama"));
                peminjamans.setPetugas(petugass);

                return peminjamans;
            }

        });

        return listPeminjaman;
    }

    @Override
    public Peminjaman get(int peminjamanId) {
        String sql = "SELECT * FROM peminjaman, buku, anggota, petugas "+
                "WHERE peminjaman.id_buku = buku.id_buku " +
                "AND peminjaman.id_anggota = anggota.id_anggota " +
                "AND peminjaman.id_petugas = petugas.id_petugas " +
                "AND id_peminjaman=" + peminjamanId;

        return jdbcTemplate.query(sql, new ResultSetExtractor<Peminjaman>() {

            @Override
            public Peminjaman extractData(ResultSet rs) throws SQLException,
                    DataAccessException {
                if (rs.next()) {
                    Peminjaman peminjaman = new Peminjaman();

                    peminjaman.setIdPeminjaman(rs.getInt("id_peminjaman"));
                    peminjaman.setKodePeminjaman(rs.getString("kode_peminjaman"));
                    peminjaman.setTanggalPinjam(rs.getString("tanggal_peminjaman"));
                    peminjaman.setTanggalKembali(rs.getString("tanggal_kembali"));

                    Buku bukus = new Buku();
                    bukus.setId_buku(rs.getInt("id_buku"));
                    bukus.setJudul(rs.getString("judul"));
                    peminjaman.setBuku(bukus);

                    Anggota anggotas = new Anggota();
                    anggotas.setIdAnggota(rs.getInt("id_anggota"));
                    anggotas.setNama(rs.getString("nama"));
                    peminjaman.setAnggota(anggotas);

                    Petugas petugass = new Petugas();
                    petugass.setId_petugas(rs.getInt("id_petugas"));
                    petugass.setNama(rs.getString("nama"));
                    peminjaman.setPetugas(petugass);

                    return peminjaman;
                }

                return null;
            }

        });
    }
}