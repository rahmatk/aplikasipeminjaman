package net.codejava.spring.dao;

import net.codejava.spring.model.Peminjaman;

import java.util.List;

/**
 * Created by yoggi on 5/23/2017.
 */
public interface PengembalianDAO {
    public void saveOrUpdate(Peminjaman peminjaman);

    public List<Peminjaman> list();

    public Peminjaman get(int peminjamanId);
}

