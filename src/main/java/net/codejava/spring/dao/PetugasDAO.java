package net.codejava.spring.dao;

import net.codejava.spring.model.Petugas;
import java.util.List;

//interface for Petugas DAO
public interface PetugasDAO {
        public void saveOrUpdate(Petugas petugas);
        public void delete(int petugasId);
        public Petugas get(int petugasId);
        public List<Petugas> list();
}
