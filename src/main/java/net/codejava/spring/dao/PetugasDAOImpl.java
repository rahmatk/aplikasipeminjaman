package net.codejava.spring.dao;

import net.codejava.spring.model.Petugas;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class PetugasDAOImpl implements PetugasDAO {
    private JdbcTemplate jdbcTemplate;
    public PetugasDAOImpl(DataSource dataSource) { jdbcTemplate = new JdbcTemplate(dataSource); }

    @Override
    public void saveOrUpdate(Petugas petugas) {
        if(petugas.getId_petugas() > 0){
            //if id_petugas are exist then update data
            String sql = "UPDATE petugas SET nama=? , alamat=? , kontak=? WHERE id_petugas=?";
            jdbcTemplate.update(sql, petugas.getNama(), petugas.getAlamat(), petugas.getKontak(), petugas.getId_petugas());
        }
        else{
            //if id_petugas doesn't exist then insert data
            String sql = "INSERT INTO petugas(nama,alamat,kontak) VALUES (?, ?, ?)";
            jdbcTemplate.update(sql, petugas.getNama(), petugas.getAlamat(), petugas.getKontak());
        }
    }

    @Override
    public void delete(int petugasId) {
        String sql = "DELETE FROM petugas WHERE id_petugas=?";
        jdbcTemplate.update(sql, petugasId);
    }

    @Override
    public Petugas get(int petugasId) {
        String sql = "SELECT * FROM petugas WHERE id_petugas=" + petugasId;
        return jdbcTemplate.query(sql, new ResultSetExtractor<Petugas>() {
            @Override
            public Petugas extractData(ResultSet rs) throws SQLException, DataAccessException {
                if(rs.next()){
                    Petugas petugas = new Petugas();
                    petugas.setId_petugas(rs.getInt("id_petugas"));
                    petugas.setNama(rs.getString("nama"));
                    petugas.setAlamat(rs.getString("alamat"));
                    petugas.setKontak((rs.getString("kontak")));
                    return petugas;
                }

                return null;
            }
        });

    }

    @Override
    public List<Petugas> list() {
        String sql = "SELECT * FROM petugas";
        List<Petugas> listPetugas = jdbcTemplate.query(sql, new RowMapper<Petugas>() {
            @Override
            public Petugas mapRow(ResultSet rs, int rowNum) throws SQLException {
                Petugas aPetugas = new Petugas();
                aPetugas.setId_petugas(rs.getInt("id_petugas"));
                aPetugas.setNama(rs.getString("nama"));
                aPetugas.setAlamat(rs.getString("alamat"));
                aPetugas.setKontak(rs.getString("kontak"));
                return aPetugas;
            }
        });
        return listPetugas;
    }
}
