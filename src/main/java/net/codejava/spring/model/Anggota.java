package net.codejava.spring.model;

/**
 * Created by yoggi on 5/23/2017.
 */
public class Anggota {
    private int idAnggota;
    private String nama;
    private String alamat;
    private String kontak;
    private String email;

    public Anggota(){

    }

    public Anggota(int idAnggota, String nama, String alamat, String kontak, String email) {
        this.idAnggota = idAnggota;
        this.nama = nama;
        this.alamat = alamat;
        this.kontak = kontak;
        this.email = email;
    }

    public int getIdAnggota() {
        return idAnggota;
    }

    public void setIdAnggota(int idAnggota) {
        this.idAnggota = idAnggota;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKontak() {
        return kontak;
    }

    public void setKontak(String kontak) {
        this.kontak = kontak;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
