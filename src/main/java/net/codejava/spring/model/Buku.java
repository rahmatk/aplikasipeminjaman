package net.codejava.spring.model;

public class Buku {
	private int id_buku;
	private String judul;
	private String penulis;
	private String penerbit;
	private String tahun;

	public Buku() {
	}

	public Buku(String judul, String penulis, String penerbit, String tahun) {
		this.judul = judul;
		this.penulis = penulis;
		this.penerbit = penerbit;
		this.tahun = tahun;
	}
	
	/* Set dan Get Method untuk mengambil data dari database */
	public int getId_buku() {
		return id_buku;
	}

	public void setId_buku(int id_buku) {
		this.id_buku = id_buku;
	}

	public String getJudul() {
		return judul;
	}

	public void setJudul(String judul) {
		this.judul = judul;
	}

	public String getPenulis() {
		return penulis;
	}

	public void setPenulis(String penulis) {
		this.penulis = penulis;
	}

	public String getPenerbit() {
		return penerbit;
	}

	public void setPenerbit(String penerbit) {
		this.penerbit = penerbit;
	}

	public String getTahun() {
		return tahun;
	}

	public void setTahun(String tahun) {
		this.tahun = tahun;
	}

}
