package net.codejava.spring.model;

import java.util.Date;

/**
 * Created by yoggi on 5/23/2017.
 */
public class Peminjaman {
    private int idPeminjaman;
    private String kodePeminjaman;
    private String tanggalPinjam;
    private String tanggalKembali;
    private Buku buku;
    private Anggota anggota;
    private Petugas petugas;

    public Peminjaman() {

    }

    public Peminjaman(int idPeminjaman, String kodePeminjaman, String tanggalPinjam, String tanggalKembali, Buku buku, Anggota anggota, Petugas petugas ) {
        this.idPeminjaman = idPeminjaman;
        this.kodePeminjaman = kodePeminjaman;
        this.tanggalPinjam = tanggalPinjam;
        this.tanggalKembali = tanggalKembali;
        this.buku = buku;
        this.anggota = anggota;
        this.petugas = petugas;
    }

    public int getIdPeminjaman() {
        return idPeminjaman;
    }

    public void setIdPeminjaman(int idPeminjaman) {
        this.idPeminjaman = idPeminjaman;
    }

    public String getKodePeminjaman() {return kodePeminjaman;}

    public void setKodePeminjaman(String kodePeminjaman) {
        this.kodePeminjaman = kodePeminjaman;
    }

    public String getTanggalPinjam() {
        return tanggalPinjam;
    }

    public void setTanggalPinjam(String tanggalPinjam) {
        this.tanggalPinjam = tanggalPinjam;
    }

    public String getTanggalKembali() {
        return tanggalKembali;
    }

    public void setTanggalKembali(String tanggalKembali) {
        this.tanggalKembali = tanggalKembali;
    }

    public Buku getBuku() {return buku;}

    public void setBuku(Buku buku) {
        this.buku = buku;
    }

    public Anggota getAnggota() {
        return anggota;
    }

    public void setAnggota(Anggota anggota) {
        this.anggota = anggota;
    }

    public Petugas getPetugas() {
        return petugas;
    }

    public void setPetugas(Petugas petugas) {
        this.petugas = petugas;
    }

}