package net.codejava.spring.model;

public class Petugas {
    private int id_petugas;
    private String nama;
    private String alamat;
    private String kontak;

    public void Petugas(){
    }

    public void Petugas(String nama, String alamat, String kontak){
        this.nama = nama;
        this.alamat = alamat;
        this.kontak = kontak;
    }

    /* Set and Get Method for entities */
    public int getId_petugas(){return id_petugas;}
    public void setId_petugas(int id_petugas){this.id_petugas = id_petugas;}

    public String getNama(){return nama;}
    public void setNama(String nama){this.nama = nama;}

    public String getAlamat(){return alamat;}
    public void setAlamat(String alamat){this.alamat = alamat;}

    public String getKontak(){return kontak;}
    public void setKontak(String kontak){this.kontak = kontak;}

}
