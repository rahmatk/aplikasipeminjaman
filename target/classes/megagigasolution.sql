-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.17 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for megagigasolution
CREATE DATABASE IF NOT EXISTS `megagigasolution` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `megagigasolution`;


-- Dumping structure for table megagigasolution.akseslogin
CREATE TABLE IF NOT EXISTS `akseslogin` (
  `id_akseslogin` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `keterangan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_akseslogin`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table megagigasolution.akseslogin: ~2 rows (approximately)
/*!40000 ALTER TABLE `akseslogin` DISABLE KEYS */;
INSERT INTO `akseslogin` (`id_akseslogin`, `username`, `password`, `keterangan`) VALUES
	(1, 'admin', 'admin', 'admin sistem'),
	(2, 'user1', 'user1', 'user');
/*!40000 ALTER TABLE `akseslogin` ENABLE KEYS */;


-- Dumping structure for table megagigasolution.anggota
CREATE TABLE IF NOT EXISTS `anggota` (
  `id_anggota` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(45) NOT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `kontak` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_anggota`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table megagigasolution.anggota: ~6 rows (approximately)
/*!40000 ALTER TABLE `anggota` DISABLE KEYS */;
INSERT INTO `anggota` (`id_anggota`, `nama`, `alamat`, `kontak`, `email`) VALUES
	(2, 'Amex', 'Jogja', '089731213', 'rahmat@gmail.com'),
	(3, 'Hilda Yogi', 'Jogja', '089317381y73', 'yogi@hilda.com'),
	(4, 'Unyil', 'Godean', '089381973223', 'Unyil@yahoo.com'),
	(5, 'Septian', 'Godean', '08983727628', 'septian@gmail.com'),
	(6, 'Desti', 'Kalimantan', '08972172837', 'desti@gmail.com'),
	(7, 'Nia', 'Bengkulu', '089272728381', 'nia@gmail.com');
/*!40000 ALTER TABLE `anggota` ENABLE KEYS */;


-- Dumping structure for table megagigasolution.buku
CREATE TABLE IF NOT EXISTS `buku` (
  `id_buku` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(60) NOT NULL,
  `penulis` varchar(65) DEFAULT NULL,
  `penerbit` varchar(65) DEFAULT NULL,
  `tahun` varchar(65) DEFAULT NULL,
  PRIMARY KEY (`id_buku`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table megagigasolution.buku: ~6 rows (approximately)
/*!40000 ALTER TABLE `buku` DISABLE KEYS */;
INSERT INTO `buku` (`id_buku`, `judul`, `penulis`, `penerbit`, `tahun`) VALUES
	(1, 'Java', 'Budi Atmaja', 'Telekomunikasi', '2012'),
	(2, 'Spring', 'Joko Dwi', 'Telekomunikasi', '2010'),
	(3, '.Net Framework', 'Reza Adriansya', 'Komunikasi', '2007'),
	(4, 'CodeIgniter', 'Satria', 'Indomaret', '2012'),
	(5, 'JQuery Power', 'Abi Mustajim', 'Alfamart', '2009'),
	(6, 'Satu Hari Menguasai Semua Bahasa Pemograman', 'Prof. Rizal', 'Indonesia Book', '2000');
/*!40000 ALTER TABLE `buku` ENABLE KEYS */;


-- Dumping structure for table megagigasolution.peminjaman
CREATE TABLE IF NOT EXISTS `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `kode_peminjaman` varchar(100) NOT NULL,
  `tanggal_peminjaman` varchar(50) NOT NULL,
  `tanggal_kembali` varchar(50) DEFAULT 'null',
  `id_buku` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Dumping data for table megagigasolution.peminjaman: ~5 rows (approximately)
/*!40000 ALTER TABLE `peminjaman` DISABLE KEYS */;
INSERT INTO `peminjaman` (`id_peminjaman`, `kode_peminjaman`, `tanggal_peminjaman`, `tanggal_kembali`, `id_buku`, `id_anggota`, `id_petugas`) VALUES
	(5, 'P01', '06/06/2017', 'null', 3, 3, 3),
	(7, 'P02', '06/06/2017', 'null', 3, 3, 3),
	(9, 'P03', '06/12/2017', '06/12/2017', 2, 3, 3),
	(10, 'P04', '06/06/2017', '06/12/2017', 1, 2, 1),
	(11, 'P05', '06/12/2017', '06/20/2017', 6, 6, 5);
/*!40000 ALTER TABLE `peminjaman` ENABLE KEYS */;


-- Dumping structure for table megagigasolution.petugas
CREATE TABLE IF NOT EXISTS `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(65) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `kontak` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table megagigasolution.petugas: ~4 rows (approximately)
/*!40000 ALTER TABLE `petugas` DISABLE KEYS */;
INSERT INTO `petugas` (`id_petugas`, `nama`, `alamat`, `kontak`) VALUES
	(1, 'Rahmat Kurniawan', 'Yogya', '089761563283'),
	(3, 'Udin', 'Jogja', '08937182321'),
	(4, 'Sapirudin', 'Yogyakarta', '0897367253'),
	(5, 'Sukijem', 'Bantul', '0897362625');
/*!40000 ALTER TABLE `petugas` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
